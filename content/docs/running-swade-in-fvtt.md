---
title: Running Savage Worlds in Foundry VTT
linkTitle: Running Savage Worlds
---

## Combat Tracker

The SWADE system applies a couple of modifications to Foundry’s Combat Tracker to align it with Savage Worlds’ Initiative rules.

Characters are given cards drawn from the Action Deck table.

If a character has the Level Headed, Improved Level Headed, or Hesitant active in the Tweaks settings in their Actor sheet, the Combat Tracker will take into account those features.

At the start of a new round, Action Cards are automatically drawn for each character. If a Joker was drawn in the previous round, the cards are shuffled (read: the table is reset) before Action Cards are distributed.

## Rewarding Bennies

GMs can quickly reward Bennies from the Players List in the bottom left of the screen.

To the right of each player listing is the number of Bennies they currently have. Clicking on this number increments the number of Bennies by one.

Right-clicking on any player name displays a context menu with additional options:

- **Give a Benny:** Gives the player a single benny as above.
- **Refresh Bennies:** Resets the selected player’s characters’ Bennies to their Bennies Reset value as indicated in their character sheet.
- **Refresh All Bennies:** Resets all player’s Bennies as above.

## Using Blast and Cone Templates

The Savage Worlds Adventure Edition system for Foundry adds custom Blast and Cone Templates to the Measurement Controls toolbar. The icons for the templates are as follows:

- **C:** Cone Template
- **S:** Small Blast Template
- **M:** Medium Blast Template
- **L:** Large Blast Template

For more information on how to use the area of effect templates in Foundry, see [Measurement and Templates on the Foundry VTT website](https://foundryvtt.com/article/measurement/).
