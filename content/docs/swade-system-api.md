---
title: "SWADE System API"
---

THIS IS A WORK IN PROGRESS

## SWADE Hooks

- ### `swadeChatCard(actor, item, html, userId)`
  Called when a new chat card is created. Contains the chat card author's actor, the item, and the html of the card.
- ### `swadeAction(actor, item, action, roll, userId)`
  Called when an action is called on an item card. Contains information about the chat card, the action, and the roll.

## Classes

### ItemChatCardHelper

The `ItemChatCardHelper` is a helper class originally created for Item Chat Cards. You can find the class in the global `game` object under `game.swade.itemChatCardHelper`.

#### static async handleAction

**Params**
| param | type |
|----------|------------|
| `item` | SwadeItem |
| `actor` | SwadeActor |
| `action` | string |

handleAction is the centrally used function to handle basic and additional actions. The `action` parameter has a few preconfigured options.

- formula: a basic Skill test on a weapon, shield or power
- damage: basic Damage on a weapon, shield or power
- reload: reloading a weapon

Any other strings passed will be treated as an additional action. If no action with the name/id exists then the function ends without doing anything.
