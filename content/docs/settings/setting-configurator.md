---
title: "Setting Configurator"
---

The Setting Configurator contains sections for Game Master, Actors, Vehicles, and Additional Stats.

## Game Master

**GM Bennies:** This option is where you can set the default number of bennies the GM has when a new game session starts or when bennies are refreshed. We recommend defaulting to one benny per player. Bennies for nonplayer characters are tracked on their respective character sheets.

## Actors

**Enable Conviction:** Conviction is a setting rule and not all games use Conviction. If Conviction is enabled, a field is added to character sheets where the number of Conviction tokens can be tracked.

**Adjust Pace with Wounds:** Select this option to have Pace automatically reduced 1” per Wound.

### Ammunition

See [Ammo Management]({{< relref "ammo-management.md" >}})

## Vehicles

**Vehicles Mods:** Enable Modification spaces on vehicle actor sheets.

**Vehicle Edges:** Enable the Edge section on vehicle actor sheets.

## Additional Stats

Additional Stats allows you to add custom stats for the individual character, or even for items.

![An additional statistic example](img/docs/additional_stats_example.png)

To create a statistic click the **+** button in the respective header, then set the values as you need them.

**Stat Key** You can either leave the default value or set your own. This value sets the path of how your statistic will be saved.

**Label** This is the label of the statistic that will show up on the character sheet.

**Data Type** Select the proper value for the types of data the statistic should hold. The options are:

- String: Words, phrases
- Number: any sort of number
- Boolean: A true/false or on/off value represented by a checkbox

**Has Max Value** Check this box if you want the statistic to have a maximum value, which will add a second input on the character sheet. Clicking the trashcan icon will delete the statistic.

Don't forget to save your changes when you're done!

Now that you have the statistics that you want, you can activate them by opening the character sheet, selecting the Tweaks option in the black bar at the top, and then activating it on the actor or item you want it on.
