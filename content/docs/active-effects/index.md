---
title: "Active Effects"
---

## What are Active Effects?

Active Effects (or AE for short) is a part of the core Foundry experience which allow `Items` i.e. Edges, Hindrances, equipment, etc to affect a character. This can enable such things as Edges directly affecting a characters stats, like the Fleet-Footed Edge.

### Limitations

Currently there are a few limitations to Active Effects

- You can only add, delete or edit an Active Effect on an item when it is not on a character, which means you can only make changes to the item in the sidebar.
- Active Effects can only affect the character they are on, not other `Items` the character owns. This means you cannot at this time create an Ability that changes a skill, as both are owned by the character, but not directly part of the character.
- You cannot create an active effect on an item, on a weapon for example, that changes the item itself. Even if you could, as soon as the weapon is owned, the editing of said AE, such as turning it off or on, is no longer possible

## How to use Active Effects

Active Effects broadly speaking come in two flavours

- On an `Item` such as an Edge or Hindrance
- On the character directly

For a list of [attribute keys](#attribute-keys) please look down below

### Active Effects on Actors

#### Player Characters

To add an AE to a character, go to the `Traits` page and click the **+ Add** button next to **Quick Access**.

![The Add button](quickaccess_add_button.png)

Select the type **Active Effect** and enter a name.

![Select the type Active Effect and enter a nam](dropdown_select_ae.png)

Once you click _Create New_ a new AE will be created on the character and its sheet will be opened. From there you can set an icon, duration, etc. At this point in time the AE will not actually do anything. To add an Effect to the AE navigate to the **Effects** tab and click the plus sign. This will add an empty change you can fill out. In most cases you can leave the change mode to _Add_, which adds (or subtracts) the entered value from the property selected by the attribute key.

- To toggle the AE click the on/off symbol next to its name in the list
- To edit the AE, click the edit icon next to its name in the list
- To delete the AE, click the trash bin icon next to its name in the list

#### NPC Characters

Working with Active Effects on NPCs works exactly the same as on player characters, except that the AE have their own section on the NPC sheet and click on the **+** symbol there will quickly create a new AE.

### Active Effects on Items

> You can currently add Active Effects to every type of Item except _Skills_. You can only add, delete or > edit an Active Effect on an item when it is not on a character, which means you can only make changes to > the item in the sidebar.

To add an Active Effect to an `Item` first find the _Active Effects_ header on the item sheet. If the item has an _Actions & Effects_ tab then you can find it there. To add an Active Effect simply click the **+** Icon at the right side of the header.

![The Add button](itemsheet_add_ae_button.png)

Clicking this icon will add a new Active Effect and open it's sheet so you can add the information and effects you require. For a list of [attribute keys](#attribute-keys) please look down below.

## Attribute Keys

- General
  - Default Bennies: `data.bennies.max`
  - Current Bennies: `data.bennies.value`
  - Max Wounds: `data.wounds.max`
  - Current Wounds: `data.wounds.value`
  - Max Fatigue: `data.fatigue.max`
  - Current Fatigue: `data.fatigue.value`
  - Ignored Wounds: `data.wounds.ignored`
  - Running Die: `data.stats.speed.runningDie`
  - Running Modifier: `data.stats.speed.runningMod`
  - Wealth Die
    - Die Sides: `data.details.wealth.die`
    - Modifier: `data.details.wealth.modifier`
    - Wild-Die Sides: `data.details.wealth.wild-die`
- Initiative
  - Level Headed: `data.initiative.hasLevelHeaded`
  - Improved Level Headed: `data.initiative.hasImpLevelHeaded`
  - Hesitant: `data.initiative.hasHesitant`
  - Quick: `data.initiative.hasQuick`
- Status
  - Shaken: `data.status.isShaken`
  - Distracted: `data.status.isDistracted`
  - Vulnerable: `data.status.isVulnerable`
  - Stunned: `data.status.isStunned`
  - Entangled: `data.status.isEntangled`
  - Bound: `data.status.isBound`
- Attributes
  - Die Type: `data.attributes.<attribute>.die.sides`
  - Modifier: `data.attributes.<attribute>.die.modifier`
  - Encumbrance die step modifier: `data.attributes.strength.encumbranceSteps`
  - Unshake bonus (currently not in use): `data.attributes.spirit.unShakeBonus`
  - Animal Smarts: `data.attributes.smarts.animal`
- Derived Stats
  - Size: `data.stats.size`
  - Pace: `data.stats.speed.value`
  - Parry: `data.stats.parry.value`
  - Toughness: `data.stats.toughness.value`
  - Armor: `data.stats.toughness.armor`
  - Parry mod and toughness mod become obsolete with the introduction of Active Effects
