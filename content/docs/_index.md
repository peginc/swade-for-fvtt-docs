---
title: "Docs"
linkTitle: "Documentation"
type: docs
menu:
  main:
    weight: 3
    pre: <i class='fas fa-book'></i>
cascade:
  - type: "docs"
---

This is the documentation for the official _Savage Worlds Adventure Edition_ game system for Foundry VTT.

# Contents

- [System Settings]({{< relref "ammo-management.md" >}})
- [Items]({{< relref "items.md" >}})
- [Character Sheet]({{< relref "character-sheet.md" >}})
- [NPC Sheet]({{< relref "npc-sheet.md" >}})
- [Vehicle Sheet]({{< relref "vehicle-sheet.md" >}})
- [Running Savage Worlds in Foundry VTT]({{< relref "running-swade-in-fvtt.md" >}})
- [Rolling Inline Roll Attributes]({{< relref "inline-roll-attributes.md" >}})
